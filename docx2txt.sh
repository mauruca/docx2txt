#!/bin/bash
path=${1%/*}
file=${1##*/}
ext="${file##*.}"
echo "path=$path file=$file ext=$ext"

echo "" >> $path/...
echo "$file" >> $path/...

#unzip -p "$1" word/document.xml | python3 -c 'import html, sys; [print(html.unescape(l), end="") for l in sys.stdin]' | sed -e 's/<\/w:p>/\n/g' | sed -e 's/<[^>]\{1,\}>//g; s/[^[:print:]]\{1,\}//g' >> $path/...

unzip -p "$1" word/document.xml | sed -e 's/<\/w:p>/\n/g' | sed -e 's/<[^>]\{1,\}>//g; s/[^[:print:]]\{1,\}//g' | python3 -c 'import html, sys; [print(html.unescape(l), end="") for l in sys.stdin]' >> $path/...

echo ""
cat $path/...

echo ""
mkdir -p .remove/$path
mv --backup=numbered "$1" .remove/$path/__.$ext

echo ""
ls .remove/$path
echo ""
